<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        App\User::truncate();
        factory('App\User')->create(
            [
                'name' => 'Renan',
                'email' => 'renan@stormtech.com.br',
                'password' => bcrypt(123456),
                'remember_token' => str_random(10),
            ]
        );

        $this->call('PostsTableSeeder');
        $this->call('TagsTableSeeder');

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
