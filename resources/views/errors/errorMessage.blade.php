@extends('template')

@section('title')
    Algum erro aconteceu!
@stop

@section('content')

<div class="alert alert-danger" role="alert">{{ $errorText }}</div>

@stop