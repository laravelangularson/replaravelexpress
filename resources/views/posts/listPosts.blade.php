@extends('template')

@section('title')
    Postagens
@stop

@section('content')

<div class="container">

    <div class="row">

        <div class="col-md-8">

            <h1 class="page-header">Postagens</h1>

            @foreach($postsPage as $post)

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ $post['Assunto'] }}</h3>
                    </div>
                    <div class="panel-body">{{ $post['Texto'] }}</div>
                </div>

            @endforeach

            <ul class="pager">
                <li class="previous">
                    <a href="#">&larr; Anterior</a>
                </li>
                <li class="next">
                    <a href="#">Próxima &rarr;</a>
                </li>
            </ul>

        </div>

        <div class="col-md-4">

            <div class="well">
                <h4>Procurar:</h4>
                <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </div>

            <div class="well">
                <h4>Categorias:</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            <li><a href="#">Programação</a></li>
                            <li><a href="#">Livros</a></li>
                            <li><a href="#">Categoria1</a></li>
                            <li><a href="#">Categoria2</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            <li><a href="#">Programação</a></li>
                            <li><a href="#">Livros</a></li>
                            <li><a href="#">Categoria1</a></li>
                            <li><a href="#">Categoria2</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@stop