<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	// Permite que utilize o metodo create. Deixa esses dados mais vuneraveis
    protected $fillable =[
        'title',
        'content'
    ];

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function tags()
    {
        // Relacionamento de muitos com muitos com model tag
        // Tabela que faz relação: posts_tags
        return $this->belongsToMany('App\Tag', 'posts_tags');
    }

    /*
     * Obrigatório "get" no inicio e "attribute" no final
     * Utilizado na view "edit" do admin.posts
     *
     */
    public function getTagListAttribute()
    {
        $tags = $this->tags()->lists('name')->all();
        return implode(', ', $tags);
    }
}
