<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;

class PostsController extends Controller
{

	public function index(){
		$posts = \App\Post::all();

		return view('posts.index', compact('posts'));
	}
	/*
	 * PostsController::listPostsPerPage
	 *
	 * @param int $page
	 * @return void
	 */
    public function listPostsPerPage( $page )
    {
		$posts = $this->getPostsFixed();
	    if ( array_key_exists($page, $posts) )
	    {
		    return view('posts.listPosts', ['postsPage' => $posts[$page] ]);
	    }
	    else return view('errors.errorMessage', ['errorText' => "A Página que você tentou acessar, não existe!" ]);
    }

	/*
	 * PostsController::getPostsFixed
	 *
	 * @return array $posts
	 */
	private function getPostsFixed()
	{
		$posts[] = array(
			array(
				'Assunto' => "Golpe no Facebook faz 10 mil vítimas baixarem vírus em dois dias",
				'Texto' => "Um novo vírus que se espalha pelo Facebook infectou mais de 10 mil pessoas dois dias após ter sido lançado, no final de junho, a maior parte delas no Brasil e outros países da América Latina."
			),
			array(
				'Assunto' => 'Mudanças em padrão Wi-Fi podem melhorar conexão e diminuir interferências',
				'Texto' => 'Uma atualização no padrão AC da tecnologia Wi-Fi vai permitir a inclusão de recursos que prometem deixar o formato de comunicação sem fio mais rápido e confiável, segundo anúncio da Wi-Fi Alliance, empresa que gerencia como a rede wireless opera.'
			)
		);

		$posts[] = array(
			array(
				'Assunto' => "Doodle Carmen Costa",
				'Texto' => "Carmen Costa, cantora e compositora brasileira, recebe hoje (5 de julho) um Doodle Google em homenagem ao dia em que faria 96 anos."
			),
			array(
				'Assunto' => 'Dercy Gonçalves ganha homenagem em Doodle do Google',
				'Texto' => 'Dercy Gonçalves ganhou uma homenagem em um Doodle Google no dia em que faria 109 anos (23 de junho).'
			)
		);

		return $posts;
	}


}
