<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	// Permite que utilize o metodo create. Deixa esses dados mais vuneraveis
	protected $fillable =[
		'post_id',
		'comment',
		'name',
		'email'
	];

	public function post()
	{
		// Percente a um "Post"
		return $this->belongsTo('App\Post');
	}
}
